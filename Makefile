SOURCES  = $(patsubst %,%.cpp,main)
OBJS     = $(SOURCES:%.cpp=%.o)
CFLAGS   = -O2 -Wall
LDFLAGS  = -s -mwindows
CXX      = i686-w64-mingw32-g++
STRIP    = i686-w64-mingw32-strip
TARGET   = wheel.exe

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS)
	$(STRIP) $@

.objs/%.o: src/%.cpp | .objs
	$(CXX) $(CGLAGS) -c $< -o $@
