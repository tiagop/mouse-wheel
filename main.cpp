#include <windows.h>

const char* UNIQUE_APP_NAME = "mouse-wheel-shaker";

void setup_tray()
{
	NOTIFYICONDATA nd;
	nd.cbSize = sizeof(NOTIFYICONDATA);
	nd.hWnd = NULL;
	nd.uID = 5000;
	nd.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;

	//Shell_NotifyIcon(NIM_ADD, &MyTrayNotifyStruct)
}

int CALLBACK WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR cmd,
	int show
)
{
	HANDLE hMutex = CreateMutex(NULL, FALSE, UNIQUE_APP_NAME);
	if (hMutex == NULL || GetLastError() == ERROR_ALREADY_EXISTS) {
		return -1;
	}

	setup_tray();

	short delta = WHEEL_DELTA;

	while (true) {
		HWND hWnd = GetForegroundWindow();
		RECT r;
		GetClientRect(hWnd, &r);
		PostMessage(
			hWnd,
			WM_MOUSEWHEEL,
			MAKEWPARAM(0, delta),
			MAKELPARAM(r.right / 2, r.bottom / 2)
		);
		delta = delta * -1;
		Sleep(2000);
	}

	ReleaseMutex(hMutex);
}
